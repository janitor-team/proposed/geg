/* help.c
 * geg, a GTK+ Equation Grapher
 * David Bryant 1998
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "help.h"
#include "i18n.h"
#include <string.h>

/* locally global data */

static GtkWidget *wi = NULL;
static void ok_event(GtkWidget *widget, gpointer data);
static gint delete_event(GtkWidget *widget, GdkEvent *event, gpointer data);
static FILE * help_file_open(gchar *message);
  
static void
ok_event(GtkWidget *widget, gpointer data)
{
  gtk_widget_destroy(wi);
  wi = NULL;
}

static gint
delete_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{
  gtk_widget_destroy(wi);
  wi = NULL;
  return(TRUE);
}

static FILE * help_file_open(gchar *message)
{
  FILE *fp;
  gchar *ptr;  
  gchar filename[256];
  
  sprintf(filename, "/usr/share/geg/help/help.%s", g_getenv("LANG"));
  ptr = strrchr(filename, '_');
  if (ptr) *ptr = '\0';
  fp = fopen(filename, "r");
  if (!fp) {
    strcpy(filename, "/usr/share/geg/help/help.en");
    fp = fopen(filename, "r");
  }
  if (!fp) {
    sprintf(message, "%s %s", filename, _("not found"));    
  }
  return fp;
}

void print_help()
{
  gchar line[512];  
  FILE *fp;
  fp = help_file_open(line);
  if (fp) {
    while(fgets(line, 256, fp)) {
      printf("%s", line);
    }
    fclose(fp);
  } else {
    printf("%s", line);
  }  
}

void
help_event(GtkWidget *widget, gpointer data)
{
  static GdkFont *font = NULL;
  GtkWidget *bu, *vb, *te, *sw;
  GtkTextBuffer *te_buf;
  GtkTextIter te_iter;
  PangoFontDescription *font_desc;
  gchar line[512];
  FILE *fp;

  if(wi && wi->window) {
    gtk_widget_map(wi);
    gdk_window_raise(wi->window);
    return;
  }

  if(!font)
    font = gdk_font_load("fixed");

  g_assert(font != NULL);

  wi = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize(wi, 960, 720);

  gtk_signal_connect(GTK_OBJECT(wi), "delete_event",
                     GTK_SIGNAL_FUNC(delete_event), NULL);

  gtk_window_set_wmclass(GTK_WINDOW(wi), "help", "Geg");
  sprintf(line, "%s: %s", "Geg", _("Help"));
  gtk_window_set_title(GTK_WINDOW(wi), line);
  gtk_window_set_policy(GTK_WINDOW(wi), TRUE, TRUE, TRUE);
  gtk_window_position(GTK_WINDOW(wi), GTK_WIN_POS_NONE);

  vb = gtk_vbox_new(FALSE, 0);
  te = gtk_text_view_new();

  font_desc = pango_font_description_from_string ("Dejavu Sans Mono 12");
  gtk_widget_modify_font (te, font_desc);

  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(te), GTK_WRAP_WORD);

  sw = gtk_scrolled_window_new(GTK_TEXT_VIEW(te)->vadjustment,
			       GTK_TEXT_VIEW(te)->hadjustment);
  gtk_container_add(GTK_CONTAINER(wi), vb);
  gtk_container_add(GTK_CONTAINER(vb), sw);
  gtk_container_add(GTK_CONTAINER(sw), te);

  bu = gtk_button_new_with_label(_("Ok"));
  gtk_widget_set_usize(bu, 50, 30);
  GTK_WIDGET_SET_FLAGS(bu, GTK_CAN_DEFAULT);
  gtk_box_pack_start(GTK_BOX(vb), sw, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(vb), bu, FALSE, FALSE, 0);
  gtk_signal_connect(GTK_OBJECT(bu), "clicked",
                     GTK_SIGNAL_FUNC(ok_event), NULL);

  te_buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(te));
  gtk_text_buffer_get_end_iter(te_buf, &te_iter);

  gtk_widget_show(wi);
  gtk_widget_show(vb);
  gtk_widget_show(sw);
  gtk_widget_show(bu);
  gtk_widget_show(te);
  gtk_widget_show((GtkWidget *)te_buf);

  fp = help_file_open(line);
  if (fp) {
    while(fgets(line, 256, fp)) {
      gtk_text_buffer_insert(te_buf, &te_iter, line, strlen(line));
    }
    fclose(fp);
  } else {
    gtk_text_buffer_insert(te_buf, &te_iter, line, strlen(line));
  }
}
