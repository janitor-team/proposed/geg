#ifndef __PARSER_H__
#define __PARSER_H__

#include "tokeniser.h"

#include <glib.h>

struct _parse_node {
  gint type;
  struct _parse_node *left;
  struct _parse_node *right;
};

typedef struct _parse_node parse_tree;

typedef struct {
  void * ptr;
  gint n;
  gdouble c;
  gdouble A;
} struct_integ;

extern struct_integ integr[26];

extern parse_tree *make_parse_tree(token_list *list);
extern gchar *check_tree(parse_tree *tree);
extern void free_tree(parse_tree *tree);
extern parse_tree *dup_tree(parse_tree *tree);
extern double eval_tree(parse_tree *tree);

#endif
