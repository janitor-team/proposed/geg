/* colors.c
 * geg, a GTK+ Equation Grapher
 * David Bryant 1998
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include "colors.h"
#include "prefs.h"

extern struct_prefs prefs;

/* alloc_color, convenience function to allocate a color from vals
 */
void
alloc_color(GdkColor *color, gdouble *vals, GdkColormap *colormap)
{
  color->red   = (gushort)(65535 * vals[0]);
  color->green = (gushort)(65535 * vals[1]);
  color->blue  = (gushort)(65535 * vals[2]);

  g_assert(colormap != NULL);
  g_assert(gdk_color_alloc(colormap, color) != 0);
}

/* alloc_func_colors, allocates the colors to be used for the functions
 */
void alloc_func_colors(GdkColormap *colormap)
{
  int i;

  for(i = 0; i < prefs.n_colors; i++)
    alloc_color(&prefs.gdk_color[i], &prefs.rgb_value[3*i], colormap); 
}

void color_print(FILE *fd, int i)
{
  fprintf(fd, "%f %f %f\n",
          prefs.rgb_value[3*i], prefs.rgb_value[3*i+1], prefs.rgb_value[3*i+2]);
}
