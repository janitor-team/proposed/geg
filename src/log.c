/* log.c
 * geg, a GTK+ Equation Grapher
 * David Bryant 1998
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>
#include "log.h"

/* locally global data */
static GtkWidget *log_te;
static GtkTextBuffer *log_te_buf;
static GtkTextIter log_te_iter;

/* create_log, create the log
 */
void
create_log(GtkWidget *widget)
{
  GtkWidget *sw;
  
  log_te = gtk_text_view_new();
  sw = gtk_scrolled_window_new(GTK_TEXT_VIEW(log_te)->vadjustment,
			       GTK_TEXT_VIEW(log_te)->hadjustment);
  gtk_container_add(GTK_CONTAINER(widget), sw);
  gtk_container_add(GTK_CONTAINER(sw), log_te);
  
  log_te_buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(log_te));
  
  gtk_widget_show(sw);  
  gtk_widget_show(log_te);
}

/* write_log, write a message to the log
 */
void
write_log(gchar *message)
{
  gtk_text_buffer_get_end_iter(log_te_buf, &log_te_iter);
  gtk_text_buffer_insert(log_te_buf, &log_te_iter, message, strlen(message));

  gtk_text_buffer_get_end_iter(log_te_buf, &log_te_iter);
  gtk_text_buffer_insert(log_te_buf, &log_te_iter, "\n", -1);
}

/* addto_log, write a message to the log without Enter character
 */
void
addto_log(gchar *message)
{
  gtk_text_buffer_get_end_iter(log_te_buf, &log_te_iter);
  gtk_text_buffer_insert(log_te_buf, &log_te_iter, message, strlen(message));
}

/* clear_log, clear the text in the log
 */
void
clear_log(GtkWidget *widget, gpointer data)
{
  gtk_text_buffer_set_text(log_te_buf, "", 0);
}
