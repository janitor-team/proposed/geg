#ifndef __APP_H__
#define __APP_H__

#include <gtk/gtk.h>
#include <gdk/gdk.h>

extern int app(void);
extern void parse_input_file(void);
extern void parse_command_line(int argc, char *argv[]);
extern void realize_preferences(GtkWidget *widget, gpointer data);
extern void set_mode(gint i);
extern void reset_variables(void);
extern void set_minmax_value(gint i);

#endif /* __APP_H__ */

