#ifndef __PREFS_H__
#define __PREFS_H__

#include <gtk/gtk.h>

extern void free_prefs_allocs();
extern void initialize_prefs();
extern void prefs_to_widgets(void);
extern void widgets_to_prefs(GtkWidget *widget, gpointer data);
extern void prefs_event(GtkWidget *widget, gpointer data);
extern GtkWidget * get_prefs_widget();
extern FILE* prefs_rc_parse(gchar *filename, gint rc);
extern FILE* prefs_rc_write(gchar *filename, gint rc);

/* default utilities */

#define DEF_EDITOR "emacs"
#define DEF_EPS_TO_PDF "ps2pdf -dEPSCrop -dPDFSETTINGS=/prepress"
#define DEF_EPS_VIEWER "evince"
#define DEF_PDF_VIEWER "evince"
#define DEF_SVG_VIEWER "inkscape"
#define DEF_EPS_TO_SVG "eps2svg"

/* rc defines */

#define GEG_RC_TOOLBAR 0
#define GEG_RC_TOOLTIPS 1
#define GEG_RC_XMIN 2
#define GEG_RC_XMAX 3
#define GEG_RC_YMIN 4
#define GEG_RC_YMAX 5
#define GEG_RC_TMIN 6
#define GEG_RC_TMAX 7
#define GEG_RC_AMIN 8
#define GEG_RC_AMAX 9
#define GEG_RC_GRAPH_WIDTH 10
#define GEG_RC_GRAPH_HEIGHT 11
#define GEG_RC_COORD_TYPE 12
#define GEG_RC_MODE_TYPE 13
#define GEG_RC_ZOOM_FACTOR 14
#define GEG_RC_NOTCH_SPACING 15
#define GEG_RC_MINIMUM_RESOLUTION 16
#define GEG_RC_MAXIMUM_RESOLUTION 17
#define GEG_RC_INTERPOLATION_FACTOR 18
#define GEG_RC_MAXIMUM_FORMULAS 19
#define GEG_RC_BACKGROUND 20
#define GEG_RC_NUMBERS 21
#define GEG_RC_AXES 22
#define GEG_RC_SELECTION 23

#define GEG_RC_PICTURES_AND_TEXT 24
#define GEG_RC_PICTURES_ONLY 25
#define GEG_RC_TEXT_ONLY 26
#define GEG_RC_ON 27
#define GEG_RC_OFF 28

#define GEG_RC_EPS 0
#define GEG_RC_PDF 1
#define GEG_RC_SVG 2

/* defaults */
#define DEF_RC_INIT 1
#define DEF_RC_FILE 0

/* toolbar stuff */
#define DEF_TB GEG_RC_PICTURES_AND_TEXT
#define DEF_TT GEG_RC_ON
#define DEF_PRINT GEG_RC_PDF

/* startup stuff */
#define DEF_XMIN -12
#define DEF_XMAX +12 
#define DEF_YMIN -9
#define DEF_YMAX +9
#define DEF_TMIN 0
#define DEF_TMAX 1
#define DEF_AMIN 0
#define DEF_AMAX 6.28318530717958647692
#define DEF_WIDTH 1024
#define DEF_HEIGHT 768
/* resolution stuff */
#define DEF_ZOOM 0.1
#define DEF_SPACE 45
#define DEF_MINRES 0.0001
#define DEF_MAXRES 10000.0
#define DEF_GAP 2.0
#define DEF_AXES_LW 0.4
#define DEF_GRID_LW 0.3
#define DEF_BOX_LW 0.4
#define DEF_CURVES_LW 0.5
#define DEF_POINT_RADIUS 1.0
#define DEF_INTERP 4
#define DEF_MAXFORM 100
#define DEF_DECIMAL 0
#define DEF_RADIAN 1
#define DEF_CARTESIAN 0
#define DEF_POLAR 1
#define DEF_PARAMETRIC 2
#define DEF_SEQUENCE 3
/* drawing */
#define DEF_DO_OX 1
#define DEF_DO_OY 1
#define DEF_DO_XVAL 1
#define DEF_DO_YVAL 1
#define DEF_DO_COLOR 1 
#define DEF_DO_BOX 0
#define DEF_DO_GRID 0
#define DEF_BACK_COLOR 0
#define DEF_NUMB_COLOR 1
#define DEF_AXES_COLOR 2
#define DEF_GRID_COLOR 3
#define DEF_SEL_COLOR 4
#define DEF_GRAY_COLOR 5
#define DEF_DEFAULT_COLOR 7
#define DEF_TEXT_POSITION 2
#define DEF_ZOOM_XY 3
#define DEF_NUMBER_SIZE 12
#define DEF_FORMULA_SIZE 20
/* font stuff */  /* this is not selectable in the prefs yet */
#define DEF_NUMB_FONT "-*-symbol-*-*-*-*-*-*-*-*-*-*-*-*"
#define DEF_TEXT_FONT "-*-helvetica-medium-r-*-*-*-*-*-*-*-*-*-*"

#define INTSTEP 26
#define RINCR 27
#define RTGTL 28
#define RTGTR 29
#define MULT 30
#define INCR 31
#define TGTL 32
#define TGTR 33
#define ACTV 34
#define GAP 35
#define LINEWIDTH 36
#define COLOR 37
#define POINTRADIUS 38
#define AMIN 39
#define AMAX 40
#define TMIN 41
#define TMAX 42
#define VIRTUAL 43
#define ENDVARIABLES 44

typedef struct {
  gdouble xmin;
  gdouble xmax;
  gdouble ymin;
  gdouble ymax;
  gdouble tmin;
  gdouble tmax;
  gdouble amin;
  gdouble amax;
  gint width;
  gint height;
  gint radian;
  gint mode;
} struct_ranges;

typedef struct {
  /* toolbar stuff */
  gint tb;
  gint tt;
  gint print;  
  /* startup stuff */
  struct_ranges ranges;
  /* drawing */
  gint do_Ox, do_Oy;
  gint do_xval, do_yval;
  gint do_color, do_box, do_grid;
  gint text_position;
  gint zoom_xy;  
  gdouble number_size;
  gdouble formula_size;  
  /* resolution stuff */
  gdouble zoom;
  gint space;
  gdouble minres;
  gdouble maxres;
  gdouble axes_linewidth;
  gdouble grid_linewidth;  
  gdouble box_linewidth;
  gdouble curves_linewidth;
  gdouble point_radius;    
  gint interp;
  gint maxform;
  /* colors */
  guint n_colors;
  guint cur_color;
  gdouble *rgb_value;
  GdkColor *gdk_color;
  /* fonts */
  gchar *text_font;
  gchar *numb_font;
  gchar *editor;
  gchar *eps_viewer;
  gchar *pdf_viewer;
  gchar *svg_viewer;
  gchar *eps_to_pdf;    
  gchar *eps_to_svg;  
} struct_prefs;

extern struct_prefs prefs;
extern gchar rc_file[256];

#endif /* __PREFS_H__ */

